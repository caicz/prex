package com.prex.common.log.feign.factory;

import com.prex.common.log.feign.RemoteLogService;
import com.prex.common.log.feign.fallback.RemoteLogFallbackImpl;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 * @Classname RemoteLogFallbackFactory
 * @Description TODO
 * @Author Created by Lihaodong (alias:小东啊) im.lihaodong@gmail.com
 * @Date 2019-08-24 10:55
 * @Version 1.0
 */
@Component
public class RemoteLogFallbackFactory implements FallbackFactory<RemoteLogService> {
    @Override
    public RemoteLogService create(Throwable throwable) {
        return new RemoteLogFallbackImpl(throwable);
    }
}
